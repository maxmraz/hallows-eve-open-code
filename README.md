## All Hallow's Eve
A little halloween game created in the Solarus Engine.
<br>
This is the open-source code for the game, not the game itself! To download the game, please visit https://maxatrillionator.itch.io/hallows-eve
<br>
Solarus engine is licensed with the GPL v3. See https://solarus-games.org for more information.
<br>
Code for this game is licensed the same, assets are copyright Max Mraz 2020, unless otherwise specified in project_db.dat
